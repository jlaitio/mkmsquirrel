import React from 'react'
import $ from 'jquery'
import _ from 'lodash'

import Slider from 'rc-slider'
import {Grid, Row, Col, Input, Table, Button} from 'react-bootstrap'

module.exports = React.createClass({
  CONDITIONS: ['M', 'NM', 'EX', 'GD', 'LP', 'PL', 'PO'],
  loadField: function(field, type, defaultValue) {
      var value = defaultValue
      if(typeof(Storage) !== 'undefined') {
          var candidate = localStorage.getItem(field)
          if (typeof(candidate) === type) {
              value = candidate
          }
      }
      return value
  },
  saveData: function(item, data) {
      if(typeof(Storage) !== 'undefined') {
          if (item === 'previousSearches') {
              var previousSearchesItem = localStorage.getItem('previousSearches')
              console.log(previousSearchesItem)
              var previousSearches = (previousSearchesItem) ? JSON.parse(previousSearchesItem) : []
              console.log(JSON.stringify(previousSearches))
              previousSearches.unshift(data)
              console.log(JSON.stringify(previousSearches))
              var newPreviousSearches = _.take(previousSearches, 5)
              console.log(JSON.stringify(newPreviousSearches))
              localStorage.setItem(item, JSON.stringify(newPreviousSearches))
              this.setState({previousSearches: newPreviousSearches})
          } else {
              localStorage.setItem(item, JSON.stringify(data))
          }
      }
  },
  getInitialState: function() {
      var minCards = 2
      var minPrice = 50
      var conditions = [0,2]
      var previousSearches = []
      if(typeof(Storage) !== 'undefined') {
          var lastSearchString = localStorage.getItem('lastSearch')
          if (lastSearchString) {
              var lastSearch = JSON.parse(lastSearchString)
              if (lastSearch.minCards) minCards = lastSearch.minCards
              if (lastSearch.minPrice) minPrice = lastSearch.minPrice
              if (lastSearch.conditions) conditions = [this.CONDITIONS.indexOf(lastSearch.conditions[0]), this.CONDITIONS.indexOf(lastSearch.conditions.slice(-1)[0])]
          }
          var previousSearchesItem = localStorage.getItem('previousSearches')
          if (previousSearchesItem) previousSearches = JSON.parse(previousSearchesItem)
      }
      return {
          minCards: minCards,
          minPrice: minPrice,
          conditions: conditions,
          cardText: '',
          previousSearches: previousSearches
      }
  },
  handleInput: function(field, value) {
    var newState = {}
    newState[field] = value
    this.setState(newState)
  },
  handleInputRef: function(field) {
    var newState = {}
    var value = this.refs[field].getValue()
    newState[field] = value
    this.setState(newState)
  },
  setCardText: function(cardText) {
    this.setState({cardText: cardText})
  },
  submit: function() {
    this.setState({loading: true})
    var cards = this.state.cardText.split('\n')
    var cardData = cards.map((c) => {
      var card
      if (c.match(/\dx? /g)) card = {name: c.substr(c.indexOf(' ')+1), count: parseInt(c.slice(0,1))}
      else card = {name: c, count: 1}
      if (card.name.endsWith('*')) {
        card.name = card.name.slice(0,-1)
        card.required = true
      }
      return card
    })

    var data = {
      minCards: this.state.minCards,
      minPrice: this.state.minPrice,
      conditions: this.CONDITIONS.slice(this.state.conditions[0], this.state.conditions[1]+1),
      cards: cardData
    }

    $.ajax({
      url: 'deals',
      method: 'POST',
      contentType: 'application/json',
      cache: false,
      data: JSON.stringify(data),
      dataType: 'json',
      success: (s) => {
        this.saveData('lastSearch', data)
        this.saveData('previousSearches', this.state.cardText)
        this.setState({deals: s, error: undefined, loading: false})
      },
      error: (e) => {
        this.setState({deals: undefined, error: e, loading: false})
      }
    })
  },
  render: function() {
      var sliderWrapperStyle = {marginTop: '30px', marginBottom: '20px'}
      var conditionMarks = {}
      for (var i = 0; i < this.CONDITIONS.length; i++) {
          conditionMarks[i] = this.CONDITIONS[i]
      }

      var i = 0
      var spinner
      if (this.state.loading) {
          spinner =
              <div className="centered sk-cube-grid">
                  <div className="sk-cube sk-cube1"></div>
                  <div className="sk-cube sk-cube2"></div>
                  <div className="sk-cube sk-cube3"></div>
                  <div className="sk-cube sk-cube4"></div>
                  <div className="sk-cube sk-cube5"></div>
                  <div className="sk-cube sk-cube6"></div>
                  <div className="sk-cube sk-cube7"></div>
                  <div className="sk-cube sk-cube8"></div>
                  <div className="sk-cube sk-cube9"></div>
              </div>
      }

      var errorElement
      if (this.state.error) {
          errorElement =
              <div className="alert alert-danger" role="alert">{this.state.error}</div>
      }
      var dealElement
      if (this.state.deals) {
          dealElement =
              <Grid>
                  <Row>
                      <Col md={6}><h2>Deals:</h2></Col>
                  </Row>
                  {this.state.deals.map((deal) => (
                      <Row key={deal.seller}>
                          <Col md={6}>
                              <h3><a href={"https://www.magiccardmarket.eu/Users/" + deal.seller}
                                     target="_blank">{deal.seller}</a> ({deal.country})</h3>
                              <Table>
                                  <tbody>
                                  <tr>
                                      <td>Price</td>
                                      <td>{(deal.price).toFixed(2)}€</td>
                                  </tr>
                                  <tr>
                                      <td>Cards</td>
                                      <td>{deal.cards.length}</td>
                                  </tr>
                                  <tr>
                                      <td>Price (% of lowest)</td>
                                      <td>{((deal.priceLevel) * 100).toFixed(2)}%</td>
                                  </tr>
                                  </tbody>
                              </Table>
                              <Table striped condensed bordered hover>
                                  <thead>
                                  <tr>
                                      <th>Card</th>
                                      <th>Condition</th>
                                      <th>Price</th>
                                      <th>Price (% of lowest)</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  {deal.cards.map((c) => (
                                      <tr key={++i}>
                                          <td>{c.name}</td>
                                          <td>{c.condition}</td>
                                          <td>{c.price}€</td>
                                          <td>{((c.priceLevel) * 100).toFixed(2)}%</td>
                                      </tr>
                                  ))}
                                  </tbody>
                              </Table>
                          </Col>
                      </Row>

                  ))}
              </Grid>
      }

      var previousSearchesElement
      var previousSearchCounter = 0
      if (this.state.previousSearches && this.state.previousSearches.length > 0) {
          previousSearchesElement =
              <div>
                  <label>Previous searches</label>
                  {this.state.previousSearches.map(previousSearch =>
                      (<Button key={++previousSearchCounter} style={{display: 'block'}} onClick={this.setCardText.bind(null, previousSearch)}>{previousSearch.substring(0, 100) + ((previousSearch.length > 100) ? '...' : '')}</Button>)
                  )}
              </div>
      }


    return (
      <Grid>
        <Row>
          <Col mdOffset={3} md={6}>
            <h1>MKMSquirrel</h1>
            <div style={sliderWrapperStyle}>
              <label htmlFor="cardSlider">Minimum amount of cards: {this.state.minCards}</label>
              <Slider id="cardSlider" max={30} defaultValue={this.state.minCards} tipTransitionName="rc-slider-tooltip-zoom-down" ref="minCards" onChange={this.handleInput.bind(null, 'minCards')} />
            </div>
            <div style={sliderWrapperStyle}>
              <label htmlFor="priceSlider">Minimum deal price: {this.state.minPrice}€</label>
              <Slider id="priceSlider" step={10} max={500} defaultValue={this.state.minPrice} tipTransitionName="rc-slider-tooltip-zoom-down" ref="minPrice" onChange={this.handleInput.bind(null, 'minPrice')} />
            </div>
            <div style={sliderWrapperStyle}>
              <label htmlFor="conditionSlider">Card condition</label>
              <Slider id="conditionSlider" range marks={conditionMarks} tipFormatter={null} max={this.CONDITIONS.length-1} defaultValue={this.state.conditions} tipTransitionName="rc-slider-tooltip-zoom-down" ref="conditions" onChange={this.handleInput.bind(null, 'conditions')} />
            </div>
            {previousSearchesElement}
            <hr/>
            <Input type="textarea" label="Cards" rows="10" placeholder="For example:&#10;Noble Hierarch&#10;2x Kitchen Finks* (the asterisk requires them to be included)&#10;2x Gavony Township" value={this.state.cardText} ref="cardText" onChange={this.handleInputRef.bind(null, 'cardText')}/>
            <Button bsStyle="primary" onClick={this.submit}>Squirrelize</Button> {spinner}
          </Col>
        </Row>
        <Row>
          <Col mdOffset={3} md={6}>
            {dealElement}
            {errorElement}
          </Col>
        </Row>
      </Grid>
  )}
});

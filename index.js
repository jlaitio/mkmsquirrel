import React from 'react'
import {render} from 'react-dom'

import Base from './Base.jsx'

render(
  <Base/>,
  document.getElementById('root')
);

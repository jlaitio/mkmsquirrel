var express = require('express')
var _ = require('lodash')
var request = require('request')
var axios = require('axios')
var fs = require('fs');
var bodyParser = require('body-parser')

var Util = require('./mkmsquirrel-util.js')

var APP_PORT = 20124
var app = express();
app.use(bodyParser.json());
app.use(express.static('assets'));

app.post('/deals', function (req, res, next) {
    var cardNames = {}
    var prices = []

    console.log('Processing request: ' + JSON.stringify(req.body))
    var minCards = req.body.minCards
    var minPrice = req.body.minPrice
    var conditions = req.body.conditions
    var cards = req.body.cards
    axios.all(
        cards.map(
          (c) => ('https://www.mkmapi.eu/ws/v1.1/output.json/products/'+encodeURIComponent(c.name.replace(/ /g,'-'))+'/1/1/true')
        ).map((cardRequestURI) => (
            axios.get(cardRequestURI, {
                headers: {
                  'Authorization': Util.getAuthorizationHeader(cardRequestURI)
                }
            }).then((response) => {
                var inGoodExpansions = response.data.product.filter((pr) => (_.includes(Util.EXPANSIONS, pr.expIcon)))
                if (inGoodExpansions.length > 0) {
                    var cheapestExpansion = _.sortBy(inGoodExpansions, (pr) => pr.priceGuide.LOWEX)[0]
                    var cardName = cheapestExpansion.name["1"].productName
                    console.log("Choosing expansion " + cheapestExpansion.expansion + " for card " + cardName)
                    cardNames[cheapestExpansion.idProduct] = cardName
                    console.log("Getting articles for " + cardName)

                    var articleRequestURI = 'https://www.mkmapi.eu/ws/v1.1/output.json/articles/'+cheapestExpansion.idProduct+''
                    return axios.get(articleRequestURI, {
                    headers: {
                      'Authorization': Util.getAuthorizationHeader(articleRequestURI)
                    }})
                }
            }).then((response) => {
                var cardId = response.data.article[0].idProduct
                var cardName = cardNames[cardId]
                var requestCard = _.find(cards, (c) => (c.name.toLowerCase() == cardName.toLowerCase()))
                console.log("Processing articles of " + cardName)

                var wanted = response.data.article.filter(
                    (a) => (a.language.idLanguage == 1 && _.includes(conditions, a.condition) && !a.isFoil && !a.isAltered && !a.isSigned)
                )
                var lowestPrice = ((wanted[0].isPlayset) ? (wanted[0].price / 4.0) : wanted[0].price)
                var wantedBySeller = _.groupBy(wanted, (a) => a.seller.username)

                Object.keys(wantedBySeller).map((seller) => {
                    var articles = wantedBySeller[seller]
                    var decomposedArticles = _.flatMap(articles, (a) => {
                        var price = ((a.isPlayset) ? (a.price / 4.0) : a.price)
                        var count = ((a.isPlayset) ? (a.count * 4) : a.count)
                        var markup = (price - lowestPrice)
                        return _.times(count, () => (
                          {"name": cardName,
                           "seller": seller,
                           "country": a.seller.country,
                           "price": price,
                           "markup": markup,
                           "priceLevel": (price/(price-markup)),
                           "condition": a.condition,
                           "required": requestCard.required
                         }
                        ))
                    })
                    var maxCountOf = _.take(decomposedArticles, requestCard.count)

                    maxCountOf.forEach((a) => {prices.push(a)})
                })
            }).catch((response) => {
                console.log("whoops1: " + response)
                console.log("whoops: " + JSON.stringify(response))
            })
        ))
    ).then((response) => {
        console.log("Data acquired, handling results")

        var bySeller = _.groupBy(prices, (p) => (p.seller))
        var result = _.flatMap(Object.keys(bySeller), (seller) => {
            var articles = bySeller[seller]

            var bestArticles = []
            var requiredArticles = articles.filter((a) => (a.required))
            requiredArticles.forEach((a) => {bestArticles.push(a)})
            var sortedArticles = _.sortBy(articles.filter((a) => (!a.required)), (a) => (a.priceLevel))
            var fillArticleCount = 0
            _.takeWhile(sortedArticles, (a) => {
                bestArticles.push(a)
                ++fillArticleCount
                return (_.sum(bestArticles.map((a) => (a.price))) < minPrice) || (bestArticles.length < minCards)
            })

            var currentPriceLevel = (_.sum(bestArticles.map((sc) => (sc.price))) / _.sum(bestArticles.map((sc) => (sc.price-sc.markup))))

            var otherArticles = _.drop(sortedArticles, fillArticleCount)
            var goodOtherArticles = otherArticles.filter((a) => (a.price < 0.5 || a.priceLevel < (currentPriceLevel + 0.15) || (a.price < 1 && a.priceLevel < 3) || (a.price < 2 && a.priceLevel < 5)))
            goodOtherArticles.forEach((a) => {bestArticles.push(a)})

            var finalPriceLevel = (_.sum(bestArticles.map((sc) => (sc.price))) / _.sum(bestArticles.map((sc) => (sc.price-sc.markup))))
            var hasAllRequired = requiredArticles.length === _.sum(cards.filter((a) => (a.required)).map((a) => (a.count)))
            if ((_.sum(bestArticles.map((a) => (a.price))) < minPrice) || (bestArticles.length < minCards) || finalPriceLevel > 1.5 || !hasAllRequired)
                return []
            else
                return [{"seller": seller,
                        "country": bestArticles[0].country,
                        "price": _.sum(bestArticles.map((a) => (a.price))),
                        "priceLevel": finalPriceLevel,
                        "cards": bestArticles.map((a) => {a.seller = undefined; a.country = undefined; return a})}]
        })
        var resultSorted = _.take(_.sortBy(result, (r) => (r.priceLevel)), 20)

        res.send(JSON.stringify(resultSorted))
    })
});

app.listen(APP_PORT, function () {
  console.log('MKMSquirrel started at port ' + APP_PORT);
});

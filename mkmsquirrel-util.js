var Config = require('./mkmsquirrel-config.js')
var crypto = require('crypto')

module.exports = {
    CONDITIONS: ['M','NM','EX'],
    EXPANSIONS: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,23,15,21,16,17,18,19,22,20,24,72,25,27,26,28,61,29,30,31,32,33,35,34,36,37,38,39,40,41,42,43,
                 44,45,46,57,49,48,47,53,52,51,50,79,54,56,67,71,81,88,89,92,96,99,101,103,104,105,190,106,110,111,112,113,115,116,117,157,159,164,
                 165,175,180,182,183,188,192,229,196,235,240,242,244,119,120,199,121,206,208,211,215,218,220,224,225,226,246,248,247,250,249,251,
                 256,255,260,259,263,327,329,356,358,366,359,367,368,374,378,382],
                 
    randomToken: function() { return (crypto.randomBytes(8).toString('hex')) },

    getAuthorizationHeader: function(requestURI) {
        var parameters = {
            oauth_consumer_key: Config.appToken,
            oauth_token: Config.accessToken,
            oauth_nonce: this. randomToken(),
            oauth_timestamp: (Math.floor(Date.now() / 1000)),
            oauth_signature_method: 'HMAC-SHA1',
            oauth_version: '1.0'
        }

        var baseString = 'GET&' + encodeURIComponent(requestURI) + '&'
        Object.keys(parameters).sort().forEach((key) => {
          baseString += key + encodeURIComponent("=" + parameters[key]) + "%26"
        })
        baseString = baseString.slice(0, -3)

        var signature = crypto.createHmac('sha1', Config.appSecret + '&' + Config.accessTokenSecret).update(baseString).digest('base64')

        var authorization = 'OAuth realm="'+requestURI+'"'
        Object.keys(parameters).sort().forEach(function(key) {
          authorization += ', ' + key + '="' + encodeURIComponent(parameters[key])+'"'
        });
        authorization += ', ' + 'oauth_signature="'+signature+'"'
        return authorization
    }

}